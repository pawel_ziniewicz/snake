package application;

import javafx.scene.image.Image;

public class Earth {
	int x;
	int y;
    final static Image earth= new Image ("jablko16.png");
	
	public Earth(int maxX, int maxY, int step){
		x=(int) (Math.random()*maxX/step)*step;
		y=(int) (Math.random()*maxY/step)*step;
	}
	
	public void change(int maxX, int maxY, int step){
		//Earth a=new Earth(maxX, maxY);
		x=(int) (Math.random()*maxX/step)*step;
		y=(int) (Math.random()*maxX/step)*step;
		
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
}
