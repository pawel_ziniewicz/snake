package application;

import java.util.Timer;
import java.util.TimerTask;

public class StopWatch {

	Timer timer;
	TimerTask timerTask;
	String stopWatch;
	int time;

   	public StopWatch(){
   		stopWatch="00:00:00";
		time=0;
		timer=new Timer();
        timerTask=new TimerTask(){
        	
        	
        	int secondsx1=0;
        	int secondsx10;
        	int minutesx1=0;
        	int minutesx10=0;
            int hoursx1=0;
            int hoursx10=0;
            //String stopWatch;
        	
        	@Override
        	public void run(){
          
        		time++;
        		secondsx1=time%10;
        		secondsx10=time%60/10;
        		minutesx1=time%600/60;
        		minutesx10=time%3600/600; 
        		hoursx1=time%36000/3600;
        		hoursx10=time%86400/36000;
        		
        		stopWatch=hoursx10+""+hoursx1+":"+minutesx10+""+minutesx1+":"+secondsx10+""+secondsx1;
        		System.out.println(stopWatch);
        		
        	}
        };
        
   	}
	
		public void start() {
			
	        long a=1000;
	        timer.schedule(timerTask, a, a); 
        
		}
		
		public int getTime(){
			return time;
		}
		
		public String getStopWatch(){
			return stopWatch;
		}
		
	
		
		public void restartTime(){
			time=0;
		}
		
		public void cancelStopWatch(){
			timer.cancel();
		}
		
		
		

	}



