package application;

import javafx.scene.image.Image;

public class Snake {

	int maxX;
	int maxY;
	int x;
	int y;
    
    final static Image sprite = new Image ("glowa16.png");
	final static int STEP=16;
	
	
	public Snake(){

	}

	public Snake(int maxX, int maxY){
		this.x=maxX/STEP/2*STEP;
		this.y=maxY/STEP/2*STEP;
		this.maxX=maxX;
		this.maxY=maxY;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}
	public void setX(int x){
		this.x=x;
	}

	public void setY(int y){
		this.y=y;
	}

	public void goUp(){
	/*	if (y<1){
		y=y-STEP+maxY;
		System.out.println("y="+y);
		System.out.println("maxY="+maxY);
		}
		else 		*/
		y=y-STEP;
		
		
	}

	public void goDown(){
		/*	if (y>maxY-STEP-1)
		y=y+STEP-maxY;
		else 		*/
		y=y+STEP;
	}

	public void goRight(){
		/*	if (x>maxX-STEP-1)
		x=x+STEP-maxX;
		else 		*/
		x=x+STEP;
	}

	public void goLeft(){
		/*	if (x<1)
		x=x-STEP+maxX;
		else		*/
		x=x-STEP;
		//System.out.println("x="+x);
		//System.out.println("maxX="+maxX);
	}
	

}
