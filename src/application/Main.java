package application;

import java.util.ArrayList;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeType;
import application.Snake;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import application.StopWatch;

public class Main extends Application {


	static GraphicsContext gc;

    String code;
    
    int maxX=320;
    int maxY=320;

    Earth planet=new Earth(maxX, maxY, application.Snake.STEP);

    ArrayList<Snake> waz=new ArrayList<Snake>();

    
    
	public void start(Stage theStage)
    {
		waz.add(new Snake(maxX, maxY));
		
        StopWatch stopWatch=new StopWatch();
        stopWatch.start();


		MenuBar menu = new MenuBar();
		Menu menuFile = new Menu("File");

        MenuItem menuFileNewGame = new MenuItem("New Game");
        MenuItem menuFileExit = new MenuItem("Exit");
        
        menu.getMenus().addAll(menuFile);
        
        
        menuFileNewGame.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent e) {
                	
                	newGame(waz, maxX, maxY, stopWatch);
                }
            }); 
        
        menuFileExit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
            	
                Platform.exit();
                stopWatch.cancelStopWatch();
            }
        }); 
        
        menuFile.getItems().addAll(menuFileNewGame);
        menuFile.getItems().addAll(menuFileExit);        


        theStage.setTitle( "SNAKE" );

      //  Group root = new Group();
        VBox root = new VBox();

     

        Canvas canvas = new Canvas( maxX,maxY );
        
        Label label=new Label("nowa kontrolka");
        
        root.getChildren().addAll( menu, canvas , label);
    //    root.getChildren().add( newGameButton );
        
        Scene theScene = new Scene( root );
        theStage.setScene( theScene );
        theStage.show();
        
        

        theScene.setOnKeyPressed(
            new EventHandler<KeyEvent>()
            {
                public void handle(KeyEvent e)
                {
                	if (!checkCrash(waz, maxX, maxY)) {
                		code = e.getCode().toString();
                		System.out.println(code+" nacisniety");
                	}
                	else System.out.println("nie mozna nic wcisnac");
                 }
            });

        gc = canvas.getGraphicsContext2D();
   
      /*  
        AnimationTimer animacja = new AnimationTimer()
        {
        	@Override
        	public void handle(long currentNanoTime)    {
        		
        		
                // Clear the canvas
        		gc.clearRect(0, 0, maxX,maxY);

        		int tempX=waz.get(0).getX();
        		int tempY=waz.get(0).getY();

        		changeDirection(waz, code);

        		checkApple(waz, planet, maxX, maxY);
        		stepping(waz, tempX, tempY);
        		if (checkCrash(waz, maxX, maxY)){
        			//System.out.println("koniecgry ++++++++++++++++++");
        			code="";
        		}
        		 
        	}
       };
        animacja.start();
        
*/
        // lepsza animacja bo nie odswierza wszyskich okienek - chodzi o rozwijalne menu, przy AnimatorTimer odswirzalo z taka
        // czestotliwoscia jak krok petli, ponizej jedno jest niezalezne od drugiego
        

        
        
        
        Timeline gameLoop = new Timeline();
        gameLoop.setCycleCount( Timeline.INDEFINITE );
        
        
        KeyFrame kf = new KeyFrame(
                Duration.seconds(0.017*10),                
                new EventHandler<ActionEvent>()
                {
                    public void handle(ActionEvent ae)
                    {
            		
                    	
                    	label.setText(stopWatch.getStopWatch());
                        // Clear the canvas
                		gc.clearRect(0, 0, maxX,maxY);

                		int tempX=waz.get(0).getX();
                		int tempY=waz.get(0).getY();

                		changeDirection(waz, code);

                		checkApple(waz, planet, maxX, maxY);
                		stepping(waz, tempX, tempY);
                		if (checkCrash(waz, maxX, maxY)){
                			//System.out.println("koniecgry ++++++++++++++++++");
                			code="";
                		}
                    }
                });
            
           gameLoop.getKeyFrames().add( kf );
            gameLoop.play();
            
           
    }

	public static void main(String[] args) {
		launch(args);
	}









	//========
public static void changeDirection(ArrayList<Snake> waz, String code){

    if (code=="LEFT") {
    	waz.get(0).goLeft();
    }

    else if(code=="RIGHT") {
    	waz.get(0).goRight();
    }

    else if(code=="UP") {
    	waz.get(0).goUp();
    }

    else if(code=="DOWN") {
    	waz.get(0).goDown();
    }

    //else{}

}
	//=========


	public static void stepping(ArrayList<Snake> waz, int w0X, int w0Y){
		if (w0X!=waz.get(0).getX() || w0Y!=waz.get(0).getY()){
			for(int i=1; i<waz.size();i++){
				int tempX=waz.get(i).getX();
				waz.get(i).setX(w0X);
				w0X=tempX;
				int tempY=waz.get(i).getY();
				waz.get(i).setY(w0Y);
				w0Y=tempY;
				gc.drawImage( application.Snake.sprite, waz.get(i).getX(), waz.get(i).getY() );



    		}
		}
		else {
			for(int i=1; i<waz.size();i++){
				gc.drawImage( application.Snake.sprite, waz.get(i).getX(), waz.get(i).getY() );

			}
		}

		gc.drawImage( application.Snake.sprite, waz.get(0).getX(), waz.get(0).getY() );
	/*	try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				*/
		

	}



	public static void checkApple(ArrayList<Snake> waz, Earth planet, int maxX, int maxY){
		if (waz.get(0).getX()==planet.getX() && waz.get(0).getY()==planet.getY()){

			boolean warunek=true;
			while(warunek){
				warunek=false;
				planet.change(maxX, maxY, application.Snake.STEP);
				for (int i=0; i<waz.size(); i++){
					if (waz.get(i).getX()==planet.getX() && waz.get(i).getY()==planet.getY()) {
						warunek=true;
					}

				}
			}


				Snake e=new Snake();
				e.setX(waz.get(waz.size()-1).getX());
				e.setY(waz.get(waz.size()-1).getY());
				waz.add(e);

		}
		gc.drawImage( application.Earth.earth, planet.getX(), planet.getY() );
	}

	public static boolean checkCrash(ArrayList<Snake> waz,int maxX, int maxY){
		boolean warunek=false;
		if  (waz.get(0).getX()<0 || (waz.get(0).getX()>maxX-application.Snake.STEP ||  waz.get(0).getY()<0 || (waz.get(0).getY()>maxY-application.Snake.STEP))){
			warunek=true;
		}
		if (waz.size()>0) {
			for (int i=1; i<waz.size(); i++){
				if (waz.get(0).getX()==waz.get(i).getX() && waz.get(0).getY()==waz.get(i).getY()) {
					warunek=true;

				}
			}
		}											
		
			return warunek;
		
	}
	
	public static void newGame(ArrayList<Snake> waz,int maxX, int maxY, StopWatch stopWatch){
		gc.clearRect(0, 0, maxX,maxY);
		waz.clear();
		waz.add(new Snake(maxX, maxY));
	    stopWatch.restartTime();
	}


}